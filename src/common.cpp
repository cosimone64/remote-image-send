/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU AGPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU AGPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include "common.hpp"
#include "zmq.hpp"

#include <cstdint>
#include <cstring>
#include <iostream>
#include <vector>

/*
 * Freestanding communication functions, used by multiple modules.
 */
using cv::Mat;
using cv::Point;
using cv::Rect;
using cv::Size;
using std::memcpy;
using std::vector;
using zmq::message_t;
using zmq::socket_t;

/*
 * Based on the contours, returns a rectangle containing the contours, in order
 * to send the relevant area only.
 */
static Rect get_rect_from_points(const vector<vector<Point>> &contours);

message_t encode_frame_msg(const FrameProperties &properties,
                           const vector<uchar> &  imgbuf) {
    message_t  frame_msg {sizeof(FrameProperties) + imgbuf.size()};
    const auto msg_data_addr = static_cast<char *>(frame_msg.data());
    memcpy(msg_data_addr, &properties, sizeof(FrameProperties));

    assert(imgbuf.size() <= MAXFRAMESIZE - sizeof(FrameProperties));
    const auto imgbuf_addr = msg_data_addr + sizeof(FrameProperties);
    memcpy(imgbuf_addr, imgbuf.data(), imgbuf.size());
    return frame_msg;
}

FrameProperties receive_frame(socket_t &socket, vector<uchar> &imgbuf) {
    message_t msg {MAXFRAMESIZE};
    socket.recv(&msg);

    const auto      msg_data_addr = static_cast<const char *>(msg.data());
    FrameProperties properties;
    memcpy(&properties, msg_data_addr, sizeof(FrameProperties));

    imgbuf.clear();
    imgbuf.reserve(properties.imgbuf_length);
    const auto imgbuf_addr = msg_data_addr + sizeof(FrameProperties);
    for (auto i = 0u; i < properties.imgbuf_length; ++i) {
        imgbuf.push_back(imgbuf_addr[i]);
    }
    return properties;
}

Rect crop_frame(const Mat &background_frame, Mat &frame) {
    Mat difference_frame;
    cv::absdiff(frame, background_frame, difference_frame);

    cv::cvtColor(difference_frame, difference_frame, cv::COLOR_RGB2GRAY);
    cv::GaussianBlur(difference_frame, difference_frame, Size {0, 0}, 4);
    cv::threshold(difference_frame, difference_frame, 0, 255, 3);
    cv::erode(difference_frame, difference_frame, cv::noArray());
    cv::dilate(difference_frame, difference_frame, cv::noArray());

    vector<vector<Point>> contours;
    cv::Canny(difference_frame, difference_frame, 0, 30, 3);
    cv::findContours(difference_frame, contours, cv::RETR_EXTERNAL,
                     cv::CHAIN_APPROX_SIMPLE);

    const auto roi = get_rect_from_points(contours);
    frame          = frame(roi);
    return roi;
}

static Rect get_rect_from_points(const vector<vector<Point>> &contours) {
    // Handle degenerate case of no contours, return a 1x1 rectangle
    Rect rect;

    if (contours.size() == 0) {
        rect = {Point {0, 0}, Point {1, 1}};
    } else {
        auto min_x = contours[0][0].x;
        auto min_y = contours[0][0].y;
        auto max_x = contours[0][0].x;
        auto max_y = contours[0][0].y;

        for (const auto &contour : contours) {
            for (const auto &point : contour) {
                if (point.x < min_x) {
                    min_x = point.x;
                }
                if (point.x > max_x) {
                    max_x = point.x;
                }
                if (point.y < min_y) {
                    min_y = point.y;
                }
                if (point.y > max_y) {
                    max_y = point.y;
                }
            }
        }

        // Handle degenerate cases.
        if (min_x == max_x) {
            ++max_x;
        }
        if (min_y == max_y) {
            ++max_y;
        }
        rect = {Point {min_x, min_y}, Point {max_x, max_y}};
    }
    return rect;
}

Mat rebuild_frame(const FrameProperties &properties, const Mat &current_frame,
                  Mat &background_frame) {
    Mat reconstructed_frame;

    if (properties.framecounter % BACKGROUND_PERIOD == 0) {
        reconstructed_frame = background_frame = current_frame;
    } else {
        const Rect roi {Point {properties.x_offset, properties.y_offset},
                        Size {properties.width, properties.height}};
        reconstructed_frame = background_frame.clone();
        current_frame.copyTo(reconstructed_frame(roi));
    }
    return reconstructed_frame;
}
