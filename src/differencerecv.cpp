/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU AGPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU AGPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

#include <unistd.h>

#include "common.hpp"
#include "zmq.hpp"

using cv::Mat;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using zmq::context_t;
using zmq::message_t;
using zmq::socket_t;

static constexpr auto default_control_port = "5555";
static constexpr auto default_data_port    = "5556";
static constexpr auto default_stream_addr  = "tcp://*:5557";
static constexpr auto server_connected_msg = "Connected to server";
static constexpr auto data_connected_msg   = "Data connected established";
static constexpr auto video_end_msg        = "Video ended";

// Forward declarations.
static void connect_socket(const string &addr, const string &port,
                           socket_t &socket);
static void send_to_streamer(const Mat &        frame_to_show,
                             const std::int32_t end_video_flag,
                             socket_t &         streaming_socket);

void receive_stream(const string &video_name, const string &addr,
                    const string &control_port, const string &data_port,
                    const string &stream_addr) {
    context_t context {1};
    socket_t  control_socket {context, ZMQ_REQ};
    socket_t  data_socket {context, ZMQ_SUB};
    socket_t  streaming_socket {context, ZMQ_PUB};

    connect_socket(addr, control_port, control_socket);
    cout << server_connected_msg << endl;

    message_t request {FILENAMESIZE};
    std::memcpy(request.data(), video_name.c_str(), FILENAMESIZE);
    control_socket.send(request);
    message_t ack_msg {4};
    control_socket.recv(&ack_msg);

    data_socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    connect_socket(addr, data_port, data_socket);
    cout << data_connected_msg << endl;

    streaming_socket.setsockopt(ZMQ_IPV6, &ipv6_flag, sizeof ipv6_flag);
    streaming_socket.bind(stream_addr.c_str());

    vector<uchar> imgbuf;
    Mat           background_frame {CAMERA_HEIGHT, CAMERA_WIDTH, CV_8UC3};
    const auto    fetch = [&] { return receive_frame(data_socket, imgbuf); };

    for (auto props = fetch(); props.framecounter >= 0; props = fetch()) {
        const auto current_frame = cv::imdecode(imgbuf, cv::IMREAD_UNCHANGED);
        const auto rebuilt_frame =
            rebuild_frame(props, current_frame, background_frame);
        send_to_streamer(rebuilt_frame, 0, data_socket);
    }
    send_to_streamer(Mat {1, 1, CV_8UC3}, -1, data_socket);
    cout << video_end_msg << endl;
}

void connect_socket(const string &addr, const string &port, socket_t &socket) {
    socket.setsockopt(ZMQ_IPV6, &ipv6_flag, sizeof ipv6_flag);
    const auto full_addr = "tcp://[" + addr + "]:" + port;
    socket.connect(full_addr.c_str());
}

void send_to_streamer(const Mat &        frame_to_show,
                      const std::int32_t end_video_flag,
                      socket_t &         streaming_socket) {
    vector<uchar> imgbuf;

    cv::imencode(".jpg", frame_to_show, imgbuf);
    const FrameProperties props {imgbuf.size(),
                                 static_cast<std::int16_t>(frame_to_show.cols),
                                 static_cast<std::int16_t>(frame_to_show.rows),
                                 0,
                                 0,
                                 end_video_flag};
    auto                  stream_msg = encode_frame_msg(props, imgbuf);
    streaming_socket.send(stream_msg);
}

int main(int argc, char *argv[]) {
    if (argc == 2 && string {argv[1]} == "--version") {
        cout << license_info << endl;
        return 1;
    }
    if (argc < 3) {
        fail("Use as " + string {argv[0]} + " <filename> <address>");
    }
    receive_stream(argv[1], argv[2], default_control_port, default_data_port,
                   default_stream_addr);
    return 0;
}
