/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU AGPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU AGPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <array>
#include <cassert>
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

#include <unistd.h>

#include "common.hpp"
#include "zmq.hpp"

using std::cout;
using std::endl;
using std::int16_t;
using std::int32_t;
using std::string;
using std::chrono::steady_clock;

using cv::Mat;
using cv::Point;
using cv::Rect;
using cv::VideoCapture;
using zmq::context_t;
using zmq::message_t;
using zmq::socket_t;

static constexpr std::chrono::milliseconds milli_period {FRAMEPERIOD};
static constexpr auto default_control_addr = "tcp://*:5555";
static constexpr auto default_data_addr    = "tcp://*:5556";
static constexpr auto camera_capture_id    = "0";
static constexpr auto waiting_msg          = "Waiting for request...";
static constexpr auto client_requested_msg = "Client requested video stream...";
static constexpr auto file_open_error_msg =
    "Error while opening file, maybe it doesn't exist?";
static constexpr auto video_end_message = "Video sending complete";

// Forward declarations.
static VideoCapture open_capture(const message_t &msg);
static void         send_video(VideoCapture &capture, socket_t &data_socket);
static void         send_frame(const cv::Mat &frame, const cv::Rect &roi,
                               std::int32_t framecounter, socket_t &data_socket);

void serve_videos(const string &control_addr, const string &data_addr) {
    context_t context {1};
    socket_t  control_socket {context, ZMQ_REP};
    socket_t  data_socket {context, ZMQ_PUB};

    control_socket.setsockopt(ZMQ_IPV6, &ipv6_flag, sizeof ipv6_flag);
    data_socket.setsockopt(ZMQ_IPV6, &ipv6_flag, sizeof ipv6_flag);
    control_socket.bind(control_addr.c_str());
    data_socket.bind(data_addr.c_str());

    while (true) {
        cout << waiting_msg << endl;
        message_t request_msg {FILENAMESIZE};
        control_socket.recv(&request_msg);
        cout << client_requested_msg << endl;

        auto capture = open_capture(request_msg);
        control_socket.send("ACK", 3);
        send_video(capture, data_socket);
    }
}

static VideoCapture open_capture(const message_t &msg) {
    constexpr auto framerate = 1000 / FRAMEPERIOD;
    const string   filename {static_cast<const char *>(msg.data())};
    VideoCapture   capture;

    if (filename == camera_capture_id) {
        capture.open(0);
        capture.set(cv::CAP_PROP_FRAME_WIDTH, CAMERA_WIDTH);
        capture.set(cv::CAP_PROP_FRAME_HEIGHT, CAMERA_HEIGHT);
        capture.set(cv::CAP_PROP_FPS, framerate);
    } else {
        capture.open(filename);
    }
    if (!capture.isOpened()) {
        fail(file_open_error_msg);
    }
    return capture;
}

static void send_video(VideoCapture &capture, socket_t &data_socket) {
    auto start_time = steady_clock::now();
    Mat  background_frame {Mat::zeros(CAMERA_HEIGHT, CAMERA_WIDTH, CV_8UC3)};
    auto current_frame = background_frame.clone();
    auto framecounter  = 0;

    while (!current_frame.empty()) {
        const auto current_time = steady_clock::now();
        const auto delta_time   = current_time - start_time;
        const auto can_send_frame =
            std::chrono::duration_cast<std::chrono::milliseconds>(delta_time)
            >= milli_period;
        if (can_send_frame) {
            capture >> current_frame;
            start_time += milli_period; // TODO: Is this correct to set the new
                                        // starting point?
            Rect roi;

            if (framecounter % BACKGROUND_PERIOD == 0) {
                background_frame = current_frame;
            } else {
                roi = crop_frame(background_frame, current_frame);
            }
            send_frame(current_frame, roi, framecounter, data_socket);
            ++framecounter;
        } else {
            capture.grab();
        }
    }
    send_frame(Mat {1, 1, CV_8UC3}, cv::Rect {}, -1,
               data_socket); // Signals termination
    cout << video_end_message << endl;
}

void send_frame(const Mat &frame, const cv::Rect &roi, const int framecounter,
                socket_t &data_socket) {
    std::vector<uchar> imgbuf;
    cv::imencode(".jpg", frame, imgbuf);

    const FrameProperties properties {imgbuf.size(),
                                      static_cast<int16_t>(frame.cols),
                                      static_cast<int16_t>(frame.rows),
                                      static_cast<int16_t>(roi.tl().x),
                                      static_cast<int16_t>(roi.tl().y),
                                      static_cast<int32_t>(framecounter)};
    auto                  frame_msg = encode_frame_msg(properties, imgbuf);
    data_socket.send(frame_msg);
}

int main(int argc, char *argv[]) {
    if (argc == 2 && string {argv[1]} == "--version") {
        cout << license_info << endl;
        return 1;
    }
    serve_videos(default_control_addr, default_data_addr);
    return 0;
}
