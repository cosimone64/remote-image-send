/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <cassert>
#include <cstring>
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>

#include "common.hpp"
#include "zmq.hpp"

using std::cout;
using std::endl;
using std::string;
using std::vector;

static constexpr auto default_port           = "5557";
static constexpr auto default_window_name    = "Camera stream";
static constexpr auto receiver_connected_msg = "Connected to receiver";
static constexpr auto video_end_msg          = "Video ended";

void stream_video(const string &addr, const string &port,
                  const string &window_name) {
    zmq::context_t context {1};
    zmq::socket_t  socket {context, ZMQ_SUB};

    socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
    socket.setsockopt(ZMQ_IPV6, &ipv6_flag, sizeof ipv6_flag);
    const auto full_addr = "tcp://[" + addr + "]:" + port;
    socket.connect(full_addr.c_str());
    cout << receiver_connected_msg << endl;

    constexpr auto imgmode = cv::IMREAD_UNCHANGED;
    vector<uchar>  imgbuf;

    while (receive_frame(socket, imgbuf).framecounter >= 0) {
        const auto frame = cv::imdecode(imgbuf, imgmode);
        cv::imshow(window_name, frame);
        cv::waitKey(FRAMEPERIOD); // Should this be reduced?
    }
    cout << video_end_msg << endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        fail("Use as " + string {argv[0]} + " <address>");
    }
    stream_video(argv[1], default_port, default_window_name);
    return 0;
}
