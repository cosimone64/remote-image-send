/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <opencv2/opencv.hpp>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <zmq.hpp>

#include <unistd.h>

#include "common.hpp"

/*
 * The *receiver* acts as an active client. It connects to
 * the server, which sends the image.
 */
constexpr std::uint8_t FRAMERATE {20};
constexpr char ADDRESS[] {"tcp://localhost:5555"};
constexpr char DATAADDRESS[] {"tcp://localhost:5556"};
constexpr char OUTPUTVIDEONAME[] {"output.avi"};
constexpr char WINDOWNAME[] {"Camera stream"};

struct PropertiesHeader
{
	int width;
	int height;
	int fourcc;
};

PropertiesHeader receive_properties(zmq::socket_t &socket);

cv::VideoWriter get_output_caputure();

void receive_video(zmq::socket_t &socket);

int main(int argc, char *argv[])
{
	if (argc < 2)
		fail("Use as " + std::string {argv[0]} + " <filename>");
	zmq::context_t context {1};
	zmq::socket_t socket {context, ZMQ_REQ};
	std::cout << "Connecting to server..." << std::endl;
	socket.connect(ADDRESS); // Using localhost for now, need to change later!

	zmq::message_t request {FILENAMESIZE};
	memcpy(request.data(), argv[1], FILENAMESIZE);
	socket.send(request);
	//const PropertiesHeader properties {receive_properties(socket)};

	zmq::message_t payload_frame_msg {MAXFRAMESIZE}; // Allocating it outside loop for now
	zmq::socket_t data_socket {context, ZMQ_SUB};
	data_socket.setsockopt(ZMQ_SUBSCRIBE, "", 0);
	data_socket.connect(DATAADDRESS);
	
	receive_video(data_socket);
	return 0;
}

PropertiesHeader receive_properties(zmq::socket_t &socket)
{
	zmq::message_t video_property_msg {sizeof(int)};
	socket.recv(&video_property_msg);
	const int width {*(static_cast<int *>(video_property_msg.data()))};
	socket.recv(&video_property_msg);
	const int height {*(static_cast<int *>(video_property_msg.data()))};
	socket.recv(&video_property_msg);
	const int fourcc {*(static_cast<int *>(video_property_msg.data()))};

	zmq::message_t properties_ack_msg;
	socket.send(properties_ack_msg);
	return PropertiesHeader {width, height, fourcc};
}

cv::VideoWriter get_output_caputure()
{
	//TODO: Need to choose a proper fourcc!
	cv::VideoWriter output_video {OUTPUTVIDEONAME,
		CV_FOURCC('Y', 'V', '1', '2'), 30,
		cv::Size {256, 240}}; //IMPORTANT: MUST PROGRAMMATICALLY GET THE EXACT DIMENSIONS!!!
	if (!output_video.isOpened())
		fail("Error opening output video");
	return output_video;
}

void receive_video(zmq::socket_t &socket)
{
	cv::VideoWriter output_video {get_output_caputure()};
	zmq::message_t payload_frame_msg {MAXFRAMESIZE}; // Allocating it outside loop for now
	std::vector<std::uint8_t> imgbuf (MAXFRAMESIZE);
	std::uint32_t framecounter {0};

	while (1) { // TODO: Use a cleaner exit instead of break!
		socket.recv(&payload_frame_msg);
		std::memcpy(imgbuf.data(), payload_frame_msg.data(), MAXFRAMESIZE);
		if (!std::memcmp(imgbuf.data(), "END", 3)) {
			std::cout << "Video ended" << std::endl;
			break;
		}
		cv::Mat frame {cv::imdecode(imgbuf, CV_LOAD_IMAGE_UNCHANGED)};
		std::cout << "Frame received " << framecounter++ << std::endl;
		cv::imshow(WINDOWNAME, frame);
		output_video.write(frame);
		cv::waitKey(FRAMERATE);
	}
}
