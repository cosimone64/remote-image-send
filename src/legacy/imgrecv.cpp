/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <array>
#include <cassert>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <zmq.hpp>

#include "common.hpp"

/*
 * The *receiver* acts as an active client. It connects to
 * the server, which sends the image.
 */
constexpr char ADDRESS[] {"tcp://localhost:5555"};
constexpr char OUTPUTVIDEONAME[] {"output.avi"};

int main(int argc, char *argv[])
{
	if (argc < 2)
		fail("Use as " + std::string {argv[0]} + " <filename>");
	zmq::context_t context {1};
	zmq::socket_t socket {context, ZMQ_REQ};
	socket.connect(ADDRESS); // Using localhost for now, need to change later!

	zmq::message_t request {FILENAMESIZE};
	memcpy(request.data(), argv[1], FILENAMESIZE);
	socket.send(request);

	zmq::message_t filesize_msg {sizeof(std::size_t)};
	socket.recv(&filesize_msg);
	const std::size_t filesize {*(static_cast<std::size_t *>(filesize_msg.data()))};

	zmq::message_t payload_reply_message {filesize};
	socket.recv(&payload_reply_message);

	std::cout << "File received\n";

	zmq::message_t ack_msg;
	socket.send(ack_msg);
	socket.recv(&ack_msg);

	std::ofstream output_file {OUTPUTVIDEONAME, std::ios::binary | std::ios::out};
	output_file.write(static_cast<const char *>(payload_reply_message.data()),
			filesize);
	return 0;
}
