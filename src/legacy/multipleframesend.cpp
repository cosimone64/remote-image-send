/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <array>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>
#include <zmq.hpp>

#include <unistd.h>

#include "common.hpp"

/*
 * The *sender* acts as a passive server. it waits
 * for a client to connect, then sends it the video.
 */
constexpr auto ADDRESS     = "tcp://*:5555";
constexpr auto DATAADDRESS = "tcp://*:5556";

void send_properties(cv::VideoCapture &input_file, zmq::socket_t &socket)
{
	int width {static_cast<int>(input_file.get(CV_CAP_PROP_FRAME_WIDTH))};
	zmq::message_t width_msg {&width, sizeof(int), nullptr};
	socket.send(width_msg);

	int height {static_cast<int>(input_file.get(CV_CAP_PROP_FRAME_HEIGHT))};
	zmq::message_t height_msg {&height, sizeof(int), nullptr};
	socket.send(height_msg);

	int fourcc {static_cast<int>(input_file.get(CV_CAP_PROP_FOURCC))};
	zmq::message_t fourcc_msg {&fourcc, sizeof(int), nullptr};
	socket.send(fourcc_msg);

	std::array<char, 3> ackbuf;
	socket.recv(ackbuf.data(), ackbuf.size());
}

void send_video(cv::VideoCapture &input_file, zmq::socket_t &socket)
{
	//cv::namedWindow("Culo", CV_WINDOW_AUTOSIZE);
	std::vector<std::uint8_t> imgbuf;
	std::uint32_t framecounter {0};
	while (1) { // TODO: Use a cleaner exit instead of break!
		cv::Mat frame;
		input_file >> frame;
		zmq::message_t payload_frame_msg {MAXFRAMESIZE}; // Should allocate less...
		if (frame.empty()) {
			socket.send("END", 3);
			break;
		}
		cv::imencode(".jpg", frame, imgbuf);
		std::memcpy(payload_frame_msg.data(), imgbuf.data(), imgbuf.size());
		socket.send(payload_frame_msg);
		std::cout << "Vector size: " << imgbuf.size() << std::endl;
		std::cout << "Frame sent " << framecounter++ << std::endl;
	}
	std::cout << "Video sending complete" << std::endl;
}

int main()
{
	zmq::context_t context {1};
	zmq::socket_t socket {context, ZMQ_REP};
	socket.bind(ADDRESS);

	while (1) {
		std::cout << "Waiting for request..." << std::endl;
		zmq::message_t request_msg {FILENAMESIZE};
		socket.recv(&request_msg);

		socket.send("ACK", 3);
		zmq::socket_t data_socket {context, ZMQ_PUB};
		data_socket.bind(DATAADDRESS);
		cv::VideoCapture input_file {
			static_cast<const char *>(request_msg.data())};
		if (!input_file.isOpened())
			fail("Error while opening file, maybe it doesn't exist?");
		send_video(input_file, data_socket);
		
		data_socket.close();
	}
	return 0;
}
