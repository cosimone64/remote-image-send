/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU GPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU GPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#include <array>
#include <cassert>
#include <cstddef>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <zmq.hpp>

#include "common.hpp"

/*
 * The *sender* acts as a passive server. it waits
 * for a client to connect, then sends it the video.
 */
constexpr char ADDRESS[] {"tcp://*:5555"};
constexpr std::uint8_t BLOCK_SIZE {64};

static std::size_t get_file_size(std::ifstream &file)
{
	file.seekg(0, std::ios_base::end);
	int64_t filesize {file.tellg()};
	file.seekg(0, std::ios_base::beg);
	return filesize;
}

static std::vector<char> allocate_file(std::ifstream &file)
{
	std::size_t filesize {get_file_size(file)};
	std::vector<char> file_buf (filesize);

	for (std::size_t offset {0}; offset < filesize; offset += BLOCK_SIZE) {
		const std::uint8_t bytes_to_read {
			offset + BLOCK_SIZE < filesize
				? BLOCK_SIZE
				: static_cast<std::uint8_t>(filesize - offset)};
		file.read(&file_buf[offset], bytes_to_read);
	}
	return file_buf;
}

int main()
{
	zmq::context_t context {1};
	zmq::socket_t socket {context, ZMQ_REP};
	socket.bind(ADDRESS);

	while (1) {
		zmq::message_t request_msg {FILENAMESIZE};
		socket.recv(&request_msg);
		std::ifstream input_file {
			static_cast<const char *>(request_msg.data()),
			std::ios::binary | std::ios::in};
		if (input_file.fail())
			fail("Error while opening file, maybe it doesn't exist?");
		std::size_t filesize {get_file_size(input_file)};
		zmq::message_t filesize_msg {&filesize, sizeof(std::size_t), nullptr};
		socket.send(filesize_msg, ZMQ_SNDMORE);

		std::vector<char> file_buf {allocate_file(input_file)};
		assert(file_buf.size() == filesize);

		//Sending the entire video in a single message for now!
		zmq::message_t payload_reply_message {&file_buf[0], filesize, nullptr};
		socket.send(payload_reply_message);
		std::cout << "Video sending complete\n";

		zmq::message_t ack_msg;
		socket.recv(&ack_msg);
		socket.send(ack_msg);
	}
	return 0;
}
