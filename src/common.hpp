/*
 * Streaming application based on a difference segmentation algorithm.
 *
 * Copyright (C) 2018 Cosimo Agati
 *
 * This program is free software, released under the GNU AGPLv3
 * (General Public License).
 *
 * You should have received a copy of the GNU AGPLv3 with this software,
 * if not, please visit <https://www.gnu.org/licenses/gpl.html>
 */
#ifndef COMMONCONSTANTS_GUARD
#define COMMONCONSTANTS_GUARD
#include "zmq.hpp"
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

constexpr auto license_info =
    "Copyright (C) 2018-2019, Cosimo Agati\n\n"
    "This program is free software: you can redistribute it and/or modify\n"
    "it under the terms of the GNU General Public License as published by\n"
    "the Free Software Foundation, either version 3 of the License, or\n"
    "(at your option) any later version.\n\n"

    "This program is distributed in the hope that it will be useful,\n"
    "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
    "GNU General Public License for more details.\n"

    "You should have received a copy of the GNU General Public License\n"
    "along with this program.  If not, see <https://www.gnu.org/licenses/>.";

constexpr auto FILENAMESIZE      = 100;  // Number of characters
constexpr auto FRAMEPERIOD       = 1200; // Milliseconds
constexpr auto BACKGROUND_PERIOD = 10;   // Number of frames

constexpr auto CAMERA_WIDTH  = 160;
constexpr auto CAMERA_HEIGHT = 120;
constexpr auto MAXFRAMESIZE  = CAMERA_WIDTH * CAMERA_HEIGHT;

constexpr auto ipv6_flag = 1;

struct FrameProperties {
    std::uint64_t imgbuf_length;
    std::int16_t  width;
    std::int16_t  height;
    std::int16_t  x_offset;
    std::int16_t  y_offset;
    std::int32_t  framecounter;
};

inline void fail(const std::string &s) {
    std::cerr << s << '\n';
    std::exit(-1);
}

/*
 * Freestanding communication functions, used by multiple modules.
 */
zmq::message_t encode_frame_msg(const FrameProperties &   properties,
                                const std::vector<uchar> &imgbuf);

FrameProperties receive_frame(zmq::socket_t &     socket,
                              std::vector<uchar> &imgbuf);

/*
 * Crops the frame to the region of interest, based on the relevant
 * differences with the current background frame.
 * Returns a cv::Rect object representing said region of interest.
 */
cv::Rect crop_frame(const cv::Mat &background_frame, cv::Mat &frame);

/*
 * Returns the reconstructed frame, from the properties, the current frame
 * and the current background frame.
 * The frame is reconstructed by copying the smaller, current frame, containing
 * the differences, onto the larger background frame.
 */
cv::Mat rebuild_frame(const FrameProperties &properties,
                      const cv::Mat &current_frame, cv::Mat &background_frame);
#endif
