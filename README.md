# remote-image-send

Small application to send and receive images or videos in streaming
via blocks, and by sending frame difference to reduce bandwith,
primarily for embedded devices such as the BeagleBone Black.

Sample AVIs taken from
http://www.engr.colostate.edu/me/facil/dynamics/avis.htm

Important: the current implementations make use of some C++17
exclusive features, such as *guaranteed* copy elision. It is therefore
recommended to use a C++ compiler fully supporting the C++17 standard.
